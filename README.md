# Introduction to VueJS #

Vue JS is a progressive framework for JavaScript used to build web interfaces and single-page-applications (SPA). It is one of the latest JavaScript front-end frameworks released and is currently used by many developers worldwide due to its efficiency and less complexity.
This session will introduce what Vue JS is, how to install, some basic features and capabilities of the framework such as Vue Instances, Data & Methods, Dynamic data binding, Two-way data binding, Looping, Components, and State management (VueX). This webinar session will be guiding the participants through these concepts with coding examples and live demonstrations of how each feature works.

### What is this repository for? ###

This repository contains all the source codes from the sample applications demonstrated in the Webinar - Introduction to Vue JS.
A separate branch is created for each section discussed throughout the session.

### How do I get set up? ###

You will need to have the following inorder to get started:

1. A code editor: [Visual Studio Code](https://code.visualstudio.com/)/[ATOM](https://atom.io/)(Recommended)
2. [Node JS](https://nodejs.org/en/)

It will be convenient for you if you have the following extension installed to start a live server locally when necessary.

- VS Code : [Live Server](https://marketplace.visualstudio.com/items?itemName=ritwickdey.LiveServer)
- ATOM : [atom-live-server](https://atom.io/packages/atom-live-server)

### Who do I talk to? ###

For any concerns of matters feel free to contact the Webinar Coordinators:

- Sankeeth : sankeethan@vetstoria.com 
- Yasith : yasith@vetstoria.com 
